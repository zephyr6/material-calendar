package com.github.naturs.calendar;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        Calendar calendar = Calendar.getInstance();
        int firstDayOfWeek = Calendar.MONDAY;
        calendar.setFirstDayOfWeek(firstDayOfWeek);
        
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DAY_OF_WEEK, - (dayOfWeek - firstDayOfWeek));
        System.out.println(calendar.getTime());
    
        calendar.add(Calendar.DAY_OF_WEEK, 6);
    
        System.out.println(calendar.getTime());
        
        System.out.println(dayOfWeek + " " + Calendar.SUNDAY);
    }
}