package com.github.naturs.calendar.demo;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.naturs.calendar.CalendarDay;
import com.github.naturs.calendar.CalendarMode;
import com.github.naturs.calendar.DayViewInfo;
import com.github.naturs.calendar.scroll.CalendarLunarDay;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 *
 * Created by naturs on 2017/10/21.
 */
public class MyDayView extends LinearLayout {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("d", Locale.getDefault());
    
    private TextView dateTv;
    private TextView contentTv;
    
    private String content;
    private boolean state;
    
    public MyDayView(@NonNull Context context, DayViewInfo info) {
        this(context, null, 0);
        setDate(info.getDate());
        CalendarLunarDay lunarDay = CalendarLunarDay.from(info.getDate());
        if (info.isToday()) {
            setContent("今天");
        } else {
            if (lunarDay.getLunarDay() == 1) {
                setContent(lunarDay.getDisplayMonth());
            } else {
                setContent(lunarDay.getDisplayMonthDay());
            }
        }
        setToday(info.isToday());
        setMode(info.getMode());
    }
    
    public MyDayView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }
    
    public MyDayView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    
    private void init(Context context) {
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER);
        LayoutInflater.from(context).inflate(R.layout.layout_day_view, this, true);
        dateTv = (TextView) findViewById(R.id.date_tv);
        contentTv = (TextView) findViewById(R.id.content_tv);
    }
    
    public void setDate(@NonNull CalendarDay date) {
        dateTv.setText(date.getDay() + "");
    }
    
    public void setDate(String s) {
        dateTv.setText(s);
    }
    
    public void setContent(String content) {
        this.content = content;
        contentTv.setText(content);
    }
    
    public void setToday(boolean today) {
        if (today) {
            dateTv.setTextSize(18);
            dateTv.getPaint().setFakeBoldText(true);
            dateTv.setTextColor(Color.BLUE);
        } else {
            dateTv.setTextSize(14);
            dateTv.getPaint().setFakeBoldText(false);
            dateTv.setTextColor(Color.parseColor("#333333"));
        }
    }
    
    public void setMode(CalendarMode mode) {
        if (CalendarMode.MONTHS.equals(mode)) {
            dateTv.setTextColor(Color.parseColor("#333333"));
        } else {
            dateTv.setTextColor(Color.parseColor("#ffff00"));
        }
    }
    
    public void setSomeState(boolean state) {
        if (this.state == state) {
            return;
        }
        this.state = state;
        if (state) {
            contentTv.setText("OK");
        } else {
            contentTv.setText(content);
        }
    }
}
