package com.github.naturs.calendar.demo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.view.View;

import com.github.naturs.calendar.CalendarDay;
import com.github.naturs.calendar.CalendarMode;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 *
 * Created by naturs on 2017/10/22.
 */
public class XDayView extends View {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("d", Locale.getDefault());
    
    private String date;
    
    private Paint mPaint;
    
    public XDayView(Context context) {
        super(context);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.BLACK);
        mPaint.setTextSize(40);
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText(date, getWidth() / 2, getHeight() / 2, mPaint);
    }
    
    public void setDate(@NonNull CalendarDay date) {
        this.date = DATE_FORMAT.format(date.getDate());
        postInvalidate();
    }
    
    public void setDate(String s) {
        
        postInvalidate();
    }
    
    public void setContent(String content) {
    
    }
    
    public void setToday(boolean today) {
    
    }
    
    public void setMode(CalendarMode mode) {
    
    }
    
}
