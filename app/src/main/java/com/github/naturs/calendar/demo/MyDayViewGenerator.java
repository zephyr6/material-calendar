package com.github.naturs.calendar.demo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;

import com.github.naturs.calendar.CalendarDay;
import com.github.naturs.calendar.DayViewGenerator;
import com.github.naturs.calendar.DayViewInfo;

/**
 *
 * Created by naturs on 2017/10/21.
 */
public class MyDayViewGenerator implements DayViewGenerator<MyDayView> {
    @Override
    public MyDayView generate(@NonNull Context context, @NonNull LayoutInflater inflater, @NonNull DayViewInfo info) {
        return new MyDayView(context, info);
    }
}
