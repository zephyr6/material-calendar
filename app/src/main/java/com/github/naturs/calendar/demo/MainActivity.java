package com.github.naturs.calendar.demo;

import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.github.naturs.calendar.CalendarDay;
import com.github.naturs.calendar.CalendarMode;
import com.github.naturs.calendar.MaterialCalendarView;
import com.github.naturs.calendar.OnDateSelectedListener;
import com.github.naturs.calendar.scroll.McvContainer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    
    @BindView(R.id.mcv_container)
    McvContainer mContainer;
    
    @BindView(R.id.mcv_month)
    MaterialCalendarView mMonthMcv;
    
    @BindView(R.id.mcv_week)
    MaterialCalendarView mWeekMcv;
    
    MonthDayViewDecorator mMonthDecorator;
    WeekDayViewDecorator mWeekDecorator;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        
        mMonthMcv.newState()
                .isCacheCalendarPositionEnabled(true)
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .setShowWeekDays(false)
                .commit();
        
        mWeekMcv.newState()
                .isCacheCalendarPositionEnabled(true)
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .setShowWeekDays(false)
                .commit();
    
        mMonthDecorator = new MonthDayViewDecorator();
        mWeekDecorator = new WeekDayViewDecorator();
    
        mMonthMcv.addDecorator(mMonthDecorator);
        mWeekMcv.addDecorator(mWeekDecorator);
        
        mContainer.setDayViewGenerator(new MyDayViewGenerator());
    
        mContainer.setExternalMonthDateSelectedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget,
                                       @NonNull CalendarDay date,
                                       boolean selected) {
                Log.d("naturs", "onDateSelected->" + date);
            }
        });
    
        mContainer.setDefaultSelectedDate();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        
    }
    
    @OnClick(R.id.btn_start)
    void onStartClick() {
        CalendarDay currentDate = mMonthMcv.getCurrentDate();
        mMonthDecorator.testUpdateState(currentDate);
//        Debug.startMethodTracing("aaa.trace");
    }
    
    @OnClick(R.id.btn_end)
    void onEndClick() {
        Debug.stopMethodTracing();
        Toast.makeText(this, "stopMethodTracing", Toast.LENGTH_SHORT).show();
    }
}
