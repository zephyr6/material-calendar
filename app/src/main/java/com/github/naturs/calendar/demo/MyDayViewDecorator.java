package com.github.naturs.calendar.demo;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;
import android.util.Log;
import android.util.SparseBooleanArray;

import com.github.naturs.calendar.CalendarDay;
import com.github.naturs.calendar.CalendarMode;
import com.github.naturs.calendar.DayViewDecorator;
import com.github.naturs.calendar.DayViewInfo;
import com.github.naturs.calendar.scroll.CalendarLunarDay;

import java.util.Calendar;
import java.util.List;

/**
 *
 * Created by naturs on 2017/10/21.
 */
public class MyDayViewDecorator extends DayViewDecorator<MyDayView> {
    
    private SparseBooleanArray mStateArray;
    
    public MyDayViewDecorator() {
        super(200);
        mStateArray = new SparseBooleanArray();
    }
    
    @Override
    protected void decorate(@NonNull MyDayView view, @NonNull DayViewInfo info, int calendarDayKey) {
        long start = System.currentTimeMillis();
        view.setDate(info.getDate());
        
        if (info.isChecked()) {
            view.setBackgroundColor(Color.RED);
        } else {
            view.setBackgroundColor(Color.WHITE);
        }
        System.out.println("MyDayViewDecorator decorate耗时：" + (System.currentTimeMillis() - start));
    }
    
    @Override
    protected void invalidateChecked(@NonNull MyDayView view, @NonNull DayViewInfo info, int calendarDayKey) {
        if (info.isChecked()) {
            view.setBackgroundColor(Color.RED);
        } else {
            view.setBackgroundColor(Color.WHITE);
        }
    }
    
    public void testUpdateState(@NonNull CalendarDay day) {
        long start = System.currentTimeMillis();
        
        Log.d("naturs", day.toString());
        
        Calendar calendar = day.getCalendar();
        final int year = day.getYear();
        final int month = day.getMonth();
        int endDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 1; i <= endDay; i ++) {
            CalendarDay temp = CalendarDay.from(year, month, i);
            mStateArray.put(temp.hashCode(), Math.random() < 0.5);
        }
    
        for (int i = 0; i < mStateArray.size(); i ++) {
            Log.d("naturs", mStateArray.keyAt(i) + " -> " + mStateArray.valueAt(i));
        }
        
        List<MyDayView> views = findDayViewsForPager(day);
        if (views == null) {
            return;
        }
    
        for (int i = 0; i < views.size(); i ++) {
            Log.d("naturs", getCalendarKey(views.get(i)) + " ==> " + views.get(i).hashCode());
        }
        
        for (int i = 0; i < views.size(); i ++) {
            MyDayView view = views.get(i);
            int calendarKey = getCalendarKey(view);
            view.setSomeState(mStateArray.get(calendarKey));
        }
        
        Log.d("naturs", "testUpdateState耗时：" + (System.currentTimeMillis() - start) + "ms.");
    }
    
}
