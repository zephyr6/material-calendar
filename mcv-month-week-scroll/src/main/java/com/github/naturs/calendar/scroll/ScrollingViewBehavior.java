package com.github.naturs.calendar.scroll;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

import java.util.List;

/**
 * 用于滚动View上，和{@link McvContainer}搭配使用。
 *
 * Created by naturs on 2017/10/18.
 */
public class ScrollingViewBehavior extends HeaderScrollingViewBehavior {

    private static final String TAG = "ScrollingViewBehavior";

    public ScrollingViewBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    View findFirstDependency(List<View> views) {
        for (int i = 0, z = views.size(); i < z; i++) {
            View view = views.get(i);
            if (view instanceof McvContainer) {
                return view;
            }
        }
        return null;
    }

//    @Override
//    public boolean onLayoutChild(CoordinatorLayout parent, View child, int layoutDirection) {
//        int oldTop = child.getTop();
//        int oldChildHeight = child.getHeight();
//
//        parent.onLayoutChild(child, layoutDirection);
//        Log.d(TAG, "onLayoutChild1:" + child.getTop());
//
//        if (oldChildHeight > 0) {
//            child.offsetTopAndBottom(oldTop - child.getTop());
//        }
//
//        Log.d(TAG, "onLayoutChild2:" + child.getTop());
//
//        return true;
//    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof McvContainer;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        System.out.println("dependency.getHeight()->" + dependency.getHeight() + " "
                + dependency.getBottom() + " " + child.getTop());
        ViewCompat.offsetTopAndBottom(child, dependency.getBottom() - child.getTop());
        return false;
    }
    
    @Override
    int getHeaderStickyHeight(View header) {
        if (header instanceof McvContainer) {
            return ((McvContainer) header).getStickyHeight();
        }
        return super.getHeaderStickyHeight(header);
    }
}
