package com.github.naturs.calendar.scroll;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.design.widget.*;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import com.github.naturs.calendar.MaterialCalendarView;

/**
 *
 * Created by naturs on 2017/10/18.
 */
public class McvBehavior extends HeaderBehavior<MaterialCalendarView> {
    
    private static final String TAG = "McvBehavior";
    private static final Interpolator DECELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final int MAX_OFFSET_ANIMATION_DURATION = 600; // ms

    private boolean mSkipNestedPreScroll;
    private boolean mWasNestedFlung;
    private ValueAnimator mOffsetAnimator;
    
    public McvBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

//    @Override
//    public boolean onLayoutChild(CoordinatorLayout parent, MaterialCalendarView child, int layoutDirection) {
//        Log.d(TAG, "onLayoutChild1 " + child.getTop() + " " + child.getBottom());
//        int oldBottom = child.getBottom();
//        int oldChildHeight = child.getHeight();
//        parent.onLayoutChild(child, layoutDirection);
//        Log.d(TAG, "onLayoutChild2 " + child.getTop() + " " + child.getBottom());
//        if (oldChildHeight > 0) {
//            child.offsetTopAndBottom(oldBottom - child.getBottom());
//        }
//        return true;
//    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout parent, MaterialCalendarView child,
                                       View directTargetChild, View target, int nestedScrollAxes) {
        Log.d(TAG, "onStartNestedScroll: " + directTargetChild.getHeight() + " " + child.getHeight());
        final boolean started = (nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL) != 0
//                && child.hasScrollableChildren()
                && parent.getHeight() - directTargetChild.getHeight() <= child.getHeight();
        
    
        if (started && mOffsetAnimator != null) {
            // Cancel any offset animation
            mOffsetAnimator.cancel();
        }
        
        return started;
    }
    
    @Override
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, MaterialCalendarView child, View target, int dx, int dy, int[] consumed) {
        

        if (dy != 0 && !mSkipNestedPreScroll) {
            int min, max;
            if (dy < 0) { // down
                if (ViewCompat.canScrollVertically(target, 1)) {
                    return;
                }
                min = -child.getMaxScrollRange();
                max = 0;
            } else {
//                if (ViewCompat.canScrollVertically(target, -1)) {
//                    return;
//                }
                min = -child.getMaxScrollRange();
                max = 0;
            }
            consumed[1] = scroll(coordinatorLayout, child, dy, min, max);
        }
    
//        Log.d(TAG, "onNestedPreScroll, dy=" + dy + ", target=" + target.getClass().getSimpleName() + " consumed[1]=" + consumed[1]);
    }
    
    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, MaterialCalendarView child,
                               View target, int dxConsumed, int dyConsumed,
                               int dxUnconsumed, int dyUnconsumed) {
        Log.d(TAG, "onNestedScroll->" + dyUnconsumed);
        if (dyUnconsumed < 0) {
            // If the scrolling view is scrolling down but not consuming, it's probably be at
            // the top of it's content
            scroll(coordinatorLayout, child, dyUnconsumed,
                    -child.getMaxScrollRange(), 0);
            // Set the expanding flag so that onNestedPreScroll doesn't handle any events
            mSkipNestedPreScroll = true;
        } else {
            // As we're no longer handling nested scrolls, reset the skip flag
            mSkipNestedPreScroll = false;
        }
    }
    
    @Override
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, MaterialCalendarView child, View target) {
        
        // TODO: 2017/10/19
        Log.d(TAG, "onStopNestedScroll-> " + getTopBottomOffsetForScrollingSibling() );
        if (!mWasNestedFlung) {
            int offset = getTopBottomOffsetForScrollingSibling();
            if (offset < -300) {
                animateOffsetTo(coordinatorLayout, child, -child.getMaxScrollRange(), 0);
            } else {
                animateOffsetTo(coordinatorLayout, child, 0, 0);
            }
        }
    
        // Reset the flags
        mSkipNestedPreScroll = false;
        mWasNestedFlung = false;
    }
    
    @Override
    public boolean onNestedPreFling(CoordinatorLayout coordinatorLayout, MaterialCalendarView child, View target, float velocityX, float velocityY) {
        return super.onNestedPreFling(coordinatorLayout, child, target, velocityX, velocityY);
    }
    
    @Override
    public boolean onNestedFling(final CoordinatorLayout coordinatorLayout,
                                 final MaterialCalendarView child, View target, float velocityX, float velocityY,
                                 boolean consumed) {
        Log.d(TAG, "onNestedFling-> " + consumed + " " + velocityY);
        
        boolean flung = false;
        
        if (!consumed) {
            // It has been consumed so let's fling ourselves
            flung = fling(coordinatorLayout, child, -getScrollRangeForDragFling(child),
                    0, -velocityY);
        } else {
            // If we're scrolling up and the child also consumed the fling. We'll fake scroll
            // up to our 'collapsed' offset
            if (velocityY < 0) {
                // We're scrolling down
                final int targetScroll = 0;
                if (getTopBottomOffsetForScrollingSibling() < targetScroll) {
                    // If we're currently not expanded more than the target scroll, we'll
                    // animate a fling
                    animateOffsetTo(coordinatorLayout, child, targetScroll, velocityY);
                    flung = true;
                }
            } else {
                // We're scrolling up
                final int targetScroll = -child.getMaxScrollRange();
                if (getTopBottomOffsetForScrollingSibling() > targetScroll) {
                    // If we're currently not expanded less than the target scroll, we'll
                    // animate a fling
                    animateOffsetTo(coordinatorLayout, child, targetScroll, velocityY);
                    flung = true;
                }
            }
        }
        
        mWasNestedFlung = flung;
        return flung;
    }

    @Override
    boolean canDragView(MaterialCalendarView view) {
        return true;
    }
    
    @Override
    int getMaxDragOffset(MaterialCalendarView view) {
        return -view.getMaxScrollRange();
    }
    
    @Override
    int getScrollRangeForDragFling(MaterialCalendarView view) {
        return view.getMaxScrollRange();
    }
    
    @Override
    void onFlingFinished(CoordinatorLayout parent, MaterialCalendarView layout) {
        if (!mWasNestedFlung) {
            int offset = getTopBottomOffsetForScrollingSibling();
            if (offset < -300) {
                animateOffsetTo(parent, layout, -layout.getMaxScrollRange(), 0);
            } else {
                animateOffsetTo(parent, layout, 0, 0);
            }
        }
    }

    private void animateOffsetTo(final CoordinatorLayout coordinatorLayout,
                                 final MaterialCalendarView child, final int offset, float velocity) {
        final int distance = Math.abs(getTopBottomOffsetForScrollingSibling() - offset);

        final int duration;
        velocity = Math.abs(velocity);
        if (velocity > 0) {
            duration = 3 * Math.round(1000 * (distance / velocity));
        } else {
            final float distanceRatio = (float) distance / child.getHeight();
            duration = (int) ((distanceRatio + 1) * 150);
        }

        animateOffsetWithDuration(coordinatorLayout, child, offset, duration);
    }

    private void animateOffsetWithDuration(final CoordinatorLayout coordinatorLayout,
                                           final MaterialCalendarView child, int offset, int duration) {
        final int currentOffset = getTopBottomOffsetForScrollingSibling();
        if (currentOffset == offset) {
            if (mOffsetAnimator != null && mOffsetAnimator.isRunning()) {
                mOffsetAnimator.cancel();
            }
            return;
        }

        if (mOffsetAnimator == null) {
            mOffsetAnimator = new ValueAnimator();
            mOffsetAnimator.setInterpolator(DECELERATE_INTERPOLATOR);
            mOffsetAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    setHeaderTopBottomOffset(coordinatorLayout, child,
                            (Integer) animator.getAnimatedValue());
                }
            });
        } else {
            mOffsetAnimator.cancel();
        }

        mOffsetAnimator.setDuration(Math.min(duration, MAX_OFFSET_ANIMATION_DURATION));
        mOffsetAnimator.setIntValues(currentOffset, offset);
        mOffsetAnimator.start();
    }
    
    private void snapToChildIfNeeded(CoordinatorLayout parent, MaterialCalendarView child) {
        final int offset = getTopBottomOffsetForScrollingSibling();
        if (child.getTop() <= -offset && child.getBottom() >= -offset) {
        
        }
    
        int snapTop = -child.getTop();
        int snapBottom = -child.getBottom();
    }
}
