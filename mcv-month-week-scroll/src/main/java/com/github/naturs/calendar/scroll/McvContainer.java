package com.github.naturs.calendar.scroll;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.github.naturs.calendar.CalendarDay;
import com.github.naturs.calendar.DayViewDecorator;
import com.github.naturs.calendar.DayViewGenerator;
import com.github.naturs.calendar.MaterialCalendarView;
import com.github.naturs.calendar.OnDateSelectedListener;
import com.github.naturs.calendar.OnMonthChangedListener;

import java.util.Calendar;

/**
 * 同时装载月模式和周模式的{@linkplain MaterialCalendarView}，用来上下滑动切换月模式和周模式。
 * <p>
 * Created by naturs on 2017/10/21.
 */
public class McvContainer extends FrameLayout {
    
    private static final String TAG = "McvContainer";
    
    private static final long UPDATE_DELAY = 400;
    
    private static final Calendar sTempCalendar = Calendar.getInstance();
    
    private MaterialCalendarView monthMcv;
    private MaterialCalendarView weekMcv;
    // 用来控制weekMcv在当前Layout里的上下滑动
    private ViewOffsetHelper offsetHelper;
    
    private boolean isCurrentShowMonth;
    
    private OnDateSelectedListener onMonthDateSelectedListener = new OnDateSelectedListener() {
        @Override
        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull final CalendarDay date, final boolean selected) {
            if (externalMonthDateSelectedListener != null) {
                externalMonthDateSelectedListener.onDateSelected(widget, date, selected);
            }
            postUpdateDelay(new Runnable() {
                @Override
                public void run() {
                    updateSelectedDateFromMonth(date, selected);
                }
            });
        }
    };
    
    private OnDateSelectedListener onWeekDateSelectedListener = new OnDateSelectedListener() {
        @Override
        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull final CalendarDay date, final boolean selected) {
            if (externalWeekDateSelectedListener != null) {
                externalWeekDateSelectedListener.onDateSelected(widget, date, selected);
            }
            postUpdateDelay(new Runnable() {
                @Override
                public void run() {
                    updateSelectedDateFromWeek(date, selected);
                }
            });
        }
    };
    
    private OnMonthChangedListener onMonthMonthChangeListener = new OnMonthChangedListener() {
        @Override
        public void onMonthChanged(MaterialCalendarView widget, final CalendarDay date) {
            if (externalMonthChangedListener != null) {
                externalMonthChangedListener.onMonthChanged(widget, date);
            }
            
            postUpdateDelay(new Runnable() {
                @Override
                public void run() {
                    updateSelectedMonthFromMonth(date);
                }
            });
        }
    };
    
    private OnMonthChangedListener onWeekMonthChangeListener = new OnMonthChangedListener() {
        @Override
        public void onMonthChanged(MaterialCalendarView widget, final CalendarDay date) {
            if (externalWeekChangedListener != null) {
                externalWeekChangedListener.onMonthChanged(widget, date);
            }
            
            postUpdateDelay(new Runnable() {
                @Override
                public void run() {
                    updateSelectedMonthFromWeek(date);
                }
            });
        }
    };
    
    // 外部监听周、月切换的listener
    private OnMonthChangedListener externalMonthChangedListener;
    private OnMonthChangedListener externalWeekChangedListener;
    
    private OnDateSelectedListener externalMonthDateSelectedListener;
    private OnDateSelectedListener externalWeekDateSelectedListener;
    
    public McvContainer(@NonNull Context context) {
        this(context, null);
    }
    
    public McvContainer(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }
    
    public McvContainer(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        monthMcv = (MaterialCalendarView) getChildAt(0);
        weekMcv = (MaterialCalendarView) getChildAt(1);
        
        weekMcv.setVisibility(INVISIBLE);
    
        isCurrentShowMonth = true;
        
        // 日期点击监听
        monthMcv.setOnDateChangedListener(onMonthDateSelectedListener);
        weekMcv.setOnDateChangedListener(onWeekDateSelectedListener);
        
        // 左右滑动监听
        monthMcv.setOnMonthChangedListener(onMonthMonthChangeListener);
        weekMcv.setOnMonthChangedListener(onWeekMonthChangeListener);
    
        // 使用单选模式
        monthMcv.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
        weekMcv.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
    
        offsetHelper = new ViewOffsetHelper(weekMcv);
    
        setDefaultSelectedDate();
    }
    
    public void addDecorator(DayViewDecorator<? extends View> decorator) {
        monthMcv.addDecorator(decorator);
        weekMcv.addDecorator(decorator);
    }
    
    public void setDayViewGenerator(DayViewGenerator<? extends View> generator) {
        monthMcv.newState().setDayViewGenerator(generator).commit();
        weekMcv.newState().setDayViewGenerator(generator).commit();
    }
    
    public int getMaxScrollRange() {
        return monthMcv.getMaxScrollRange();
    }
    
    public int getStickyHeight() {
        return monthMcv.getStickyHeight();
    }
    
    public void setExternalMonthChangedListener(OnMonthChangedListener l) {
        this.externalMonthChangedListener = l;
    }
    
    public void setExternalWeekChangedListener(OnMonthChangedListener l) {
        this.externalWeekChangedListener = l;
    }
    
    public void setExternalMonthDateSelectedListener(OnDateSelectedListener l) {
        this.externalMonthDateSelectedListener = l;
    }
    
    public void setExternalWeekDateSelectedListener(OnDateSelectedListener l) {
        this.externalWeekDateSelectedListener = l;
    }
    
    /**
     * 设置默认选中的天数
     */
    public void setDefaultSelectedDate() {
        // 默认选中今天
        CalendarDay today = CalendarDay.today();
        monthMcv.setSelectedDate(today, isCurrentShowMonth);
        weekMcv.setSelectedDate(today, !isCurrentShowMonth);
    }
    
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    
        int weekTop, weekBottom;
        final int offsetHeight = monthMcv.findLineIndex() * monthMcv.getStickyHeight();
        weekTop = weekMcv.getTop() + offsetHeight;
        weekBottom = weekMcv.getBottom() + offsetHeight;
        weekMcv.layout(
                weekMcv.getLeft(),
                weekTop,
                weekMcv.getRight(),
                weekBottom
        );
    
        Log.d(TAG, " ");
        Log.d(TAG, "--------------");
        
        Log.e(TAG, "onLayout weekMcv top - bottom: " + weekTop + " " + weekBottom);
        offsetHelper.onViewLayout();
        if (!isCurrentShowMonth) {
            offsetHelper.setTopAndBottomOffset(getHeight() - offsetHelper.getLayoutTop() - monthMcv.getStickyHeight());
            Log.e(TAG, "onLayout 额外layout: " + offsetHelper.getTopAndBottomOffset());
        }
        
        Log.d(TAG, "月视图位置：" + monthMcv.getLeft() + " " + monthMcv.getTop() + " " + monthMcv.getRight() + " " + monthMcv.getBottom());
        Log.d(TAG, "周视图位置：" + weekMcv.getLeft() + " " + weekMcv.getTop() + " " + weekMcv.getRight() + " " + weekMcv.getBottom());
        Log.d(TAG, "当前视图位置：" + getLeft() + " " + getTop() + " " + getRight() + " " + getBottom());
    
        Log.d(TAG, "--------------");
        Log.d(TAG, " ");
    }
    
    void dispatchOffsetUpdates(int offset) {
//        Log.i(TAG, "dispatchOffsetUpdates: " + offset + " " + offsetHelper.getTopAndBottomOffset());
        int index = monthMcv.findLineIndex();
        if (index < 0) {
            return;
        }
        
        int offsetHeight = -index * monthMcv.getStickyHeight();
        
        int delta = offset - offsetHeight;
        
        if (delta >= 0) {
            offsetHelper.setTopAndBottomOffset(0);
            if (weekMcv.getVisibility() != INVISIBLE) {
                weekMcv.setVisibility(INVISIBLE);
            }
            return;
        }
        
        if (weekMcv.getVisibility() != VISIBLE) {
            weekMcv.setVisibility(VISIBLE);
        }
        offsetHelper.setTopAndBottomOffset(-delta);
//        Log.d(TAG, "周视图滚动位置：" + weekMcv.getLeft() + " " + weekMcv.getTop() + " " + weekMcv.getRight() + " " + weekMcv.getBottom());
    }
    
    /**
     * 当前控件结束滚动的时候会调用到该方法。
     * @param totalOffset 滚动的距离
     */
    void finishScroll(int totalOffset) {
        isCurrentShowMonth = totalOffset == 0;
    }
    
    /**
     * 在月视图中点击日期进行日期选择，此时需要更新周视图中选中的日期和选中的页面。
     * @param date 月视图中点击选中的日期
     * @param selected 是否被选中还是取消选中，对于单选模式下始终为true。
     */
    private void updateSelectedDateFromMonth(@NonNull CalendarDay date, boolean selected) {
        weekMcv.setOnDateChangedListener(null);
        weekMcv.setOnMonthChangedListener(null);
        
        weekMcv.setSelectedDate(date);
        weekMcv.setCurrentDate(date, false);
        
        weekMcv.setOnDateChangedListener(onWeekDateSelectedListener);
        weekMcv.setOnMonthChangedListener(onWeekMonthChangeListener);
    
        Log.d(TAG, "updateSelectedDateFromMonth-> " + date);
    }
    
    /**
     * 在周视图中点击日期进行日期选择，此时需要更新月视图中选中的日期和选中的页面。
     * @param date 周视图中点击选中的日期
     * @param selected 是否被选中还是取消选中，对于单选模式下始终为true。
     */
    private void updateSelectedDateFromWeek(@NonNull CalendarDay date, boolean selected) {
        monthMcv.setOnDateChangedListener(null);
        monthMcv.setOnMonthChangedListener(null);
        
        monthMcv.setSelectedDate(date);
        monthMcv.setCurrentDate(date, false);
        
        monthMcv.setOnDateChangedListener(onMonthDateSelectedListener);
        monthMcv.setOnMonthChangedListener(onMonthMonthChangeListener);
    }
    
    /**
     * 滑动月视图
     * @param date 当前选中的月份
     */
    private void updateSelectedMonthFromMonth(@NonNull CalendarDay date) {
        CalendarDay selectedDate = monthMcv.getSelectedDate();
        if (selectedDate == null) {
            return;
        }
        
        monthMcv.setOnDateChangedListener(null);
        monthMcv.setOnMonthChangedListener(null);
        
        // 当前选中的日期
        int dayOfMonth = selectedDate.getDay();
        // 获取当前滑动月份的最大天数
        Calendar calendar = date.getCalendar();
        int maxDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        // 设置当前滑动到的月份的日期
        calendar.set(Calendar.DAY_OF_MONTH, Math.min(dayOfMonth, maxDayOfMonth));
    
        monthMcv.setSelectedDate(calendar);
    
        monthMcv.setOnDateChangedListener(onMonthDateSelectedListener);
        monthMcv.setOnMonthChangedListener(onMonthMonthChangeListener);
        
        Log.d(TAG, "updateSelectedMonthFromMonth-> " + date);
        updateSelectedDateFromMonth(CalendarDay.from(calendar), true);
    }
    
    /**
     * 滑动周视图
     * @param date 当前选中
     */
    private void updateSelectedMonthFromWeek(@NonNull CalendarDay date) {
        CalendarDay selectedDate = weekMcv.getSelectedDate();
        if (selectedDate == null) {
            return;
        }
        
        weekMcv.setOnDateChangedListener(null);
        weekMcv.setOnMonthChangedListener(null);
        
        // 获取当前滑动月份的最大天数
        selectedDate.copyTo(sTempCalendar);
        
        // 选中的日期是星期几？
        final int selectedDayOfWeek = sTempCalendar.get(Calendar.DAY_OF_WEEK);
    
        date.copyTo(sTempCalendar);
        sTempCalendar.add(Calendar.DAY_OF_WEEK, selectedDayOfWeek - 1);

        weekMcv.setSelectedDate(sTempCalendar);
    
        weekMcv.setOnDateChangedListener(onWeekDateSelectedListener);
        weekMcv.setOnMonthChangedListener(onWeekMonthChangeListener);
        
        updateSelectedDateFromWeek(CalendarDay.from(sTempCalendar), true);
    }
    
    private void postUpdateDelay(Runnable r) {
        postDelayed(r, UPDATE_DELAY);
    }
}
