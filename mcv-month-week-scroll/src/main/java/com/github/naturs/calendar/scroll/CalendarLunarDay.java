package com.github.naturs.calendar.scroll;

import com.github.naturs.calendar.CalendarDay;

/**
 *
 * Created by naturs on 2017/10/18.
 */
public class CalendarLunarDay {
    private static final double TZ = 8.00;
    
    private static String[] ARRAY1 = { "日", "一", "二", "三", "四", "五", "六", "七",
            "八", "九", "十" };
    private static String[] ARRAY2 = { "初", "十", "廿", "三" };
    
    private static String[] MONTH = {"正月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "冬月", "腊月"};
    
    private final int lunarYear;
    private final int lunarMonth;
    private final int lunarDay;
    private final int lunarLeap; // 是否是闰年
    
    public CalendarLunarDay(int year, int month, int day) {
        int[] lunar = DateHelper.convertSolar2Lunar(day, month + 1, year, TZ);
        this.lunarYear = lunar[2];
        this.lunarMonth = lunar[1];
        this.lunarDay = lunar[0];
        this.lunarLeap = lunar[3];
    }
    
    public int getLunarYear() {
        return lunarYear;
    }
    
    public int getLunarMonth() {
        return lunarMonth;
    }
    
    public int getLunarDay() {
        return lunarDay;
    }
    
    public int getLunarLeap() {
        return lunarLeap;
    }
    
    public String getDisplayMonthDay() {
        switch (lunarDay) {
            case 10:
                return  "初十";
            case 20:
                return "二十";
            case 30:
                return "三十";
            default:
                return ARRAY2[lunarDay / 10] + ARRAY1[lunarDay % 10];// 取余
        }
    }
    
    public String getDisplayMonth() {
        return MONTH[lunarMonth - 1];
    }
    
    public static CalendarLunarDay from(CalendarDay day) {
        return new CalendarLunarDay(day.getYear(), day.getMonth(), day.getDay());
    }
}
