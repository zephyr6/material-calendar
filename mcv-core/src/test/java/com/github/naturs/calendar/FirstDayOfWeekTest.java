package com.github.naturs.calendar;

import org.junit.Test;

import java.util.Calendar;

import static java.util.Calendar.DATE;

/**
 *
 * Created by naturs on 2017/11/27.
 */
public class FirstDayOfWeekTest {
    
    @Test
    public void test() {
        final int firstDayOfWeek = Calendar.FRIDAY;
        
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(firstDayOfWeek);
        int dow = CalendarUtils.getDayOfWeek(calendar);
        int delta = firstDayOfWeek - dow;
        calendar.add(DATE, delta);
        
        System.out.println("delta = " + delta);
        
        for (int i = 0; i < 7; i ++) {
            System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
            calendar.add(Calendar.DATE, 1);
        }
    }
    
}
