package com.github.naturs.calendar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Generate a view for day item.
 *
 * Created by naturs on 2017/10/17.
 */
public interface DayViewGenerator<V extends View> {
    
    V generate(@NonNull Context context, @NonNull LayoutInflater inflater, @NonNull DayViewInfo info);

    DayViewGenerator DEFAULT = new DayViewDefaultGenerator();

    class DayViewDefaultGenerator implements DayViewGenerator<DayView> {

        @Override
        public DayView generate(@NonNull Context context, @NonNull LayoutInflater inflater, @NonNull DayViewInfo info) {
            return new DayView(context, info.getDate());
        }
    }
}
