package com.github.naturs.calendar;

import android.support.annotation.NonNull;

import static com.github.naturs.calendar.MaterialCalendarView.*;

/**
 * base info for day view item.
 * <p>
 * Created by naturs on 2017/10/17.
 */
public class DayViewInfo {
    
    @NonNull
    private final CalendarDay date;
    @NonNull
    private final CalendarMode mode;
    
    private boolean isInRange = true;
    private boolean isInMonth = true;
    @ShowOtherDates
    private int showOtherDates = SHOW_DEFAULTS;

    private boolean checked;
    
    public DayViewInfo(@NonNull CalendarDay date, @NonNull CalendarMode mode) {
        this.date = date;
        this.mode = mode;
    }
    
    public boolean isInRange() {
        return isInRange;
    }
    
    public void setInRange(boolean inRange) {
        isInRange = inRange;
    }
    
    public boolean isInMonth() {
        return isInMonth;
    }
    
    public void setInMonth(boolean inMonth) {
        isInMonth = inMonth;
    }

    public int getShowOtherDates() {
        return showOtherDates;
    }
    
    public void setShowOtherDates(int showOtherDates) {
        this.showOtherDates = showOtherDates;
    }
    
    @NonNull
    public CalendarDay getDate() {
        return date;
    }
    
    @NonNull
    public CalendarMode getMode() {
        return mode;
    }
    
    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    
    public boolean isToday() {
        return CalendarDay.today().equals(date);
    }
}
