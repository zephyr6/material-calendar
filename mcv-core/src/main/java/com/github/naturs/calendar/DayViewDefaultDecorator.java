package com.github.naturs.calendar;

import android.support.annotation.NonNull;
import android.view.View;

import com.github.naturs.calendar.format.DayFormatter;

/**
 *
 * Created by naturs on 2017/10/18.
 */
public class DayViewDefaultDecorator extends DayViewDecorator<DayView> implements View.OnClickListener {

    public static final int PRIORITY = 1;

    private DayFormatter dayFormatter = DayFormatter.DEFAULT;
    private Integer dateTextAppearance = null;
    private int selectionColor;
    private boolean selectionEnabled;

    public DayViewDefaultDecorator() {
        super(PRIORITY); // the default render
    }

    @Override
    protected void decorate(@NonNull DayView view, @NonNull DayViewInfo info, int calendarDayKey) {
        view.setupSelection(info.getShowOtherDates(), info.isInRange(), info.isInMonth());

        view.setDayFormatter(dayFormatter);
        if (dateTextAppearance != null) {
            view.setTextAppearance(view.getContext(), dateTextAppearance);
        }
        view.setSelectionColor(selectionColor);

        view.setOnClickListener(selectionEnabled ? this : null);
        view.setClickable(selectionEnabled);

        view.setChecked(info.isChecked());
    }
    
    @Override
    protected void invalidateChecked(@NonNull DayView view, @NonNull DayViewInfo info, int calendarDayKey) {
        view.setChecked(info.isChecked());
    }
    
    @Override
    public void onClick(View v) {
        onDateClick(v);
    }

    public void setDayFormatter(DayFormatter formatter) {
        this.dayFormatter = formatter == null ? DayFormatter.DEFAULT : formatter;
    }

    public void setDateTextAppearance(int taId) {
        this.dateTextAppearance = taId;
    }

    public void setSelectionColor(int color) {
        this.selectionColor = color;
    }

    public void setSelectionEnabled(boolean selectionEnabled) {
        this.selectionEnabled = selectionEnabled;
    }

}
