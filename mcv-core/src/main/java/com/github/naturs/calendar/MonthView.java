package com.github.naturs.calendar;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;
import android.view.View;

import java.util.Calendar;

/**
 * Display a month of {@linkplain DayView}s and
 * seven {@linkplain WeekDayView}s.
 */
@SuppressLint("ViewConstructor")
class MonthView extends CalendarPagerView {

    public MonthView(@NonNull MaterialCalendarView view, CalendarDay month,
                     int firstDayOfWeek, boolean showWeekDays,
                     DayViewGenerator<? extends View> dayViewGenerator) {
        super(view, month, firstDayOfWeek, showWeekDays, dayViewGenerator);
    }

    @Override
    protected void buildDayViews(SparseArrayCompat<View> dayViews, Calendar calendar) {
        for (int r = 0; r < DEFAULT_MAX_WEEKS; r++) {
            for (int i = 0; i < DEFAULT_DAYS_IN_WEEK; i++) {
                addDayView(dayViews, calendar);
            }
        }
    }

    public CalendarDay getMonth() {
        return getFirstViewDay();
    }

    @Override
    protected boolean isDayEnabled(CalendarDay day) {
        return day.getMonth() == getFirstViewDay().getMonth();
    }

    @Override
    protected int getRows() {
        return DEFAULT_MAX_WEEKS;
    }
    
    @Override
    protected CalendarMode getMode() {
        return CalendarMode.MONTHS;
    }
}
