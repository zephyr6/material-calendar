package com.github.naturs.calendar;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Calendar;

import static java.util.Calendar.DATE;

/**
 * 周标签。
 * <br>
 * Created by naturs on 2017/11/27.
 */
public class WeekDayLayout extends LinearLayout {
    protected static final int DEFAULT_DAYS_IN_WEEK = 7;
    private static final int DEFAULT_TEXT_SIZE = 12; // sp
    private static final int DEFAULT_TEXT_COLOR = Color.parseColor("#333333");
    
    private final ArrayList<WeekDayView> weekDayViews = new ArrayList<>();
    
    private int firstDayOfWeek = Calendar.SUNDAY;
    private float textSize;
    private ColorStateList colorStateList;
    private int textAppearanceId;
    
    public WeekDayLayout(Context context) {
        this(context, null);
    }
    
    public WeekDayLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }
    
    public WeekDayLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }
    
    private void init(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.WeekDayLayout, defStyleAttr, 0);
    
        firstDayOfWeek = a.getInt(R.styleable.WeekDayLayout_weekDay_firstDayOfWeek, Calendar.SUNDAY);
        textSize = a.getDimension(R.styleable.WeekDayLayout_weekDay_TextSize, 0);
        colorStateList = a.getColorStateList(R.styleable.WeekDayLayout_weekDay_TextColor);
        textAppearanceId = a.getResourceId(R.styleable.WeekDayLayout_weekDay_TextAppearance, R.style.TextAppearance_WeekDayLayout_WeekDay);
    
        a.recycle();
        
        setOrientation(HORIZONTAL);
        
        buildWeekDays(getWorkCalendar());
    }
    
    private Calendar getWorkCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(firstDayOfWeek);
        int dow = CalendarUtils.getDayOfWeek(calendar);
        int delta = firstDayOfWeek - dow;
        calendar.add(DATE, delta);
        return calendar;
    }
    
    private void buildWeekDays(Calendar calendar) {
        for (int i = 0; i < DEFAULT_DAYS_IN_WEEK; i++) {
            WeekDayView weekDayView = new WeekDayView(getContext(), CalendarUtils.getDayOfWeek(calendar));
            
            if (textAppearanceId != 0) {
                weekDayView.setTextAppearance(weekDayView.getContext(), textAppearanceId);
            }
            if (textSize != 0) {
                weekDayView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
            if (colorStateList != null) {
                weekDayView.setTextColor(colorStateList);
            }
            weekDayViews.add(weekDayView);
            addView(weekDayView, new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0F));
            calendar.add(DATE, 1);
        }
    }
    
}
