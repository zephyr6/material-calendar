package com.github.naturs.calendar;

import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.github.naturs.calendar.format.DayFormatter;
import com.github.naturs.calendar.format.WeekDayFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import static com.github.naturs.calendar.MaterialCalendarView.SHOW_DEFAULTS;
import static com.github.naturs.calendar.MaterialCalendarView.ShowOtherDates;
import static com.github.naturs.calendar.MaterialCalendarView.showOtherMonths;
import static java.util.Calendar.DATE;

abstract class CalendarPagerView extends ViewGroup implements View.OnClickListener {

    protected static final int DEFAULT_DAYS_IN_WEEK = 7;
    protected static final int DEFAULT_MAX_WEEKS = 6;
    protected static final int DAY_NAMES_ROW = 1;
    private static final Calendar tempWorkingCalendar = CalendarUtils.getInstance();
    private final ArrayList<WeekDayView> weekDayViews = new ArrayList<>();
    @ShowOtherDates
    protected int showOtherDates = SHOW_DEFAULTS;
    private MaterialCalendarView mcv;
    private CalendarDay firstViewDay;
    private CalendarDay minDate = null;
    private CalendarDay maxDate = null;
    private int firstDayOfWeek;
    private boolean showWeekDays;
    
    private DayViewGenerator<? extends View> dayViewGenerator;

    private final SparseArrayCompat<View> dayViews = new SparseArrayCompat<>();
    
    private final SparseArrayCompat<DayViewDecorator<? extends View>> dayViewDecorators = new SparseArrayCompat<>();
    private final SparseArrayCompat<DayViewInfo> dayViewInfos = new SparseArrayCompat<>();

    private final List<DayViewDecorator<View>> defaultDayViewDecorators = new ArrayList<>();

    private final LayoutInflater layoutInflater;

    public CalendarPagerView(@NonNull MaterialCalendarView view,
                             CalendarDay firstViewDay,
                             int firstDayOfWeek,
                             boolean showWeekDays,
                             @NonNull DayViewGenerator<? extends View> generator) {
        super(view.getContext());
        this.mcv = view;
        this.firstViewDay = firstViewDay;
        this.firstDayOfWeek = firstDayOfWeek;
        this.showWeekDays = showWeekDays;
        
        this.dayViewGenerator = generator;
        this.layoutInflater = LayoutInflater.from(getContext());

        setClipChildren(false);
        setClipToPadding(false);

        if (this.showWeekDays) {
            buildWeekDays(resetAndGetWorkingCalendar());
        }
        buildDayViews(dayViews, resetAndGetWorkingCalendar());

        buildDefaultDecorators();
        addDefaultDecorators();
        invalidateDecorators();
    }

    private void buildWeekDays(Calendar calendar) {
        for (int i = 0; i < DEFAULT_DAYS_IN_WEEK; i++) {
            WeekDayView weekDayView = new WeekDayView(getContext(), CalendarUtils.getDayOfWeek(calendar));
            weekDayViews.add(weekDayView);
            addView(weekDayView);
            calendar.add(DATE, 1);
        }
    }

    /**
     * create default decorators
     */
    private void buildDefaultDecorators() {
        DayViewDecorator<View> decorator = new DayViewVisibleDecorator();
        decorator.setMcv(mcv);
        defaultDayViewDecorators.add(decorator);
    }

    private void addDefaultDecorators() {
        for (DayViewDecorator<View> decorator : defaultDayViewDecorators) {
            dayViewDecorators.put(decorator.priority(), decorator);
        }
    }

    protected void addDayView(SparseArrayCompat<View> dayViews, Calendar calendar) {
        final CalendarDay day = CalendarDay.from(calendar);
        final int dayKey = day.hashCode();
    
        final DayViewInfo info = new DayViewInfo(day, getMode());
        updateDayViewInfo(info);
        dayViewInfos.put(dayKey, info);
        
        View view = dayViewGenerator.generate(getContext(), layoutInflater, info);
        if (view == null) {
            throw new NullPointerException();
        }
        if (view.getParent() != null) {
            throw new RuntimeException("The view has a parent.");
        }
    
        view.setOnClickListener(this);
        dayViews.put(dayKey, view);
        addView(view, new LayoutParams());
        
        calendar.add(DATE, 1);
    }

    protected View findDayView(@NonNull CalendarDay day) {
        return dayViews.get(day.hashCode());
    }
    
    protected int findLineIndex(@NonNull CalendarDay day) {
        int index = dayViewInfos.indexOfKey(day.hashCode());
        return index / DEFAULT_DAYS_IN_WEEK;
    }

    @SuppressWarnings("unchecked")
    <V> SparseArrayCompat<V> cloneDayViews() {
        return (SparseArrayCompat<V>) dayViews.clone();
//        SparseArrayCompat<V> array = new SparseArrayCompat<>(dayViews.size());
//        for (int i = 0; i < dayViews.size(); i ++) {
//            array.put(dayViews.keyAt(i), (V) dayViews.valueAt(i));
//        }
//        return array;
    }
    
    /**
     * 判断某一天是否在在当前页面中
     * @param day 待判断的日期
     * @return 存在返回true
     */
    boolean isInCurrentDays(@NonNull CalendarDay day) {
        for (int i = 0; i < dayViewInfos.size(); i ++) {
            if (day.equals(dayViewInfos.valueAt(i).getDate())) {
                return true;
            }
        }
        return false;
    }
    
    void findRangeDayViews(@NonNull CalendarDay startDay,
                           @NonNull CalendarDay endDay,
                           @NonNull List<View> dayViews) {
        CalendarDay temp;
        for (int i = 0, size = dayViewInfos.size(); i < size; i ++) {
            temp = dayViewInfos.valueAt(i).getDate();
            
            if (temp.isInRange(startDay, endDay)) {
                dayViews.add(this.dayViews.get(dayViewInfos.keyAt(i)));
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private void decorateDayView(View view, int dayKey, DayViewInfo info) {
        for (int i = 0; i < dayViewDecorators.size(); i ++) {
            DayViewDecorator decorator = dayViewDecorators.valueAt(i);
            // there is a type cast
            if (decorator.shouldDecorate(view, info)) {
                decorator.decorate(view, info, dayKey);
            }
        }
    }
    
    /**
     * 仅更新时间的选中状态
     */
    @SuppressWarnings("unchecked")
    private void invalidateDayViewChecked(View view, int dayKey, DayViewInfo info) {
        for (int i = 0; i < dayViewDecorators.size(); i ++) {
            DayViewDecorator decorator = dayViewDecorators.valueAt(i);
            // there is a type cast
            decorator.invalidateChecked(view, info, dayKey);
        }
    }

    protected Calendar resetAndGetWorkingCalendar() {
        getFirstViewDay().copyTo(tempWorkingCalendar);
        //noinspection ResourceType
        tempWorkingCalendar.setFirstDayOfWeek(getFirstDayOfWeek());
        int dow = CalendarUtils.getDayOfWeek(tempWorkingCalendar);
        int delta = getFirstDayOfWeek() - dow;
        //If the delta is positive, we want to remove a week
        boolean removeRow = showOtherMonths(showOtherDates) ? delta >= 0 : delta > 0;
        if (removeRow) {
            delta -= DEFAULT_DAYS_IN_WEEK;
        }
        tempWorkingCalendar.add(DATE, delta);
        return tempWorkingCalendar;
    }

    protected int getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    protected abstract void buildDayViews(SparseArrayCompat<View> dayViews, Calendar calendar);

    protected abstract boolean isDayEnabled(CalendarDay day);

    void setDayViewDecorators(@NonNull SparseArrayCompat<DayViewDecorator<? extends View>> dayViewDecorators) {
        long start = System.currentTimeMillis();
        this.dayViewDecorators.clear();
        addDefaultDecorators();
        for (int i = 0; i < dayViewDecorators.size(); i ++) {
            DayViewDecorator<? extends View> decorator = dayViewDecorators.valueAt(i);
            decorator.setMcv(mcv);
            this.dayViewDecorators.put(dayViewDecorators.keyAt(i), decorator);
        }
        
        invalidateDecorators();
        System.out.println("setDayViewDecorators " + firstViewDay + " " + (System.currentTimeMillis() - start));
    }

    public void setWeekDayTextAppearance(int taId) {
        for (WeekDayView weekDayView : weekDayViews) {
            weekDayView.setTextAppearance(getContext(), taId);
        }
    }

    @Deprecated
    public void setDateTextAppearance(int taId) {
        DayViewDecorator decorator = dayViewDecorators.get(DayViewDefaultDecorator.PRIORITY);
        if (decorator != null) {
            DayViewDefaultDecorator defaultDecorator = (DayViewDefaultDecorator) decorator;
            defaultDecorator.setDateTextAppearance(taId);
        }
    }

    public void setShowOtherDates(@ShowOtherDates int showFlags) {
        this.showOtherDates = showFlags;
        updateUi();
    }

    @Deprecated
    public void setSelectionEnabled(boolean selectionEnabled) {
        DayViewDecorator decorator = dayViewDecorators.get(DayViewDefaultDecorator.PRIORITY);
        if (decorator != null) {
            DayViewDefaultDecorator defaultDecorator = (DayViewDefaultDecorator) decorator;
            defaultDecorator.setSelectionEnabled(selectionEnabled);
        }
    }

    @Deprecated
    public void setSelectionColor(int color) {
        DayViewDecorator decorator = dayViewDecorators.get(DayViewDefaultDecorator.PRIORITY);
        if (decorator != null) {
            DayViewDefaultDecorator defaultDecorator = (DayViewDefaultDecorator) decorator;
            defaultDecorator.setSelectionColor(color);
        }
    }

    public void setWeekDayFormatter(WeekDayFormatter formatter) {
        for (WeekDayView dayView : weekDayViews) {
            dayView.setWeekDayFormatter(formatter);
        }
    }

    @Deprecated
    public void setDayFormatter(DayFormatter formatter) {
        DayViewDecorator decorator = dayViewDecorators.get(DayViewDefaultDecorator.PRIORITY);
        if (decorator != null) {
            DayViewDefaultDecorator defaultDecorator = (DayViewDefaultDecorator) decorator;
            defaultDecorator.setDayFormatter(formatter);
        }
    }
    
    public void setMinimumDate(CalendarDay minDate) {
        this.minDate = minDate;
        updateUi();
    }

    public void setMaximumDate(CalendarDay maxDate) {
        this.maxDate = maxDate;
        updateUi();
    }

    public void setSelectedDates(Collection<CalendarDay> dates) {
        for (int i = 0; i < dayViewInfos.size(); i ++) {
            int dayKey = dayViewInfos.keyAt(i);
            DayViewInfo info = dayViewInfos.valueAt(i);
            info.setChecked(dates != null && dates.contains(info.getDate()));
            invalidateDayViewChecked(dayViews.get(dayKey), dayKey, info);
        }
    }

    protected void updateDayViewInfo(DayViewInfo info) {
        CalendarDay day = info.getDate();
        info.setShowOtherDates(showOtherDates);
        info.setInRange(day.isInRange(minDate, maxDate));
        info.setInMonth(isDayEnabled(info.getDate()));
    }

    protected void updateUi() {
        for (int i = 0; i < dayViewInfos.size(); i ++) {
            DayViewInfo info = dayViewInfos.valueAt(i);
            updateDayViewInfo(info);
        }
        invalidateDecorators();
    }

    protected void invalidateDecorators() {
        if (dayViewDecorators == null) {
            throw new NullPointerException();
        }
        long start = System.currentTimeMillis();
        for (int i = 0; i < dayViewInfos.size(); i ++) {
            int dayKey = dayViewInfos.keyAt(i);
            DayViewInfo info = dayViewInfos.valueAt(i);
            if (info == null) {
                throw new NullPointerException();
            }
            decorateDayView(dayViews.get(dayKey), dayKey, info);
        }
        System.out.println("invalidateDecorators " +firstViewDay+" "  + (System.currentTimeMillis() - start) + " size:" + dayViewInfos.size());
    }

    @Override
    public void onClick(View v) {
        if (v instanceof DayView) {
            final DayView dayView = (DayView) v;
            mcv.onDateClicked(dayView);
        }
    }

    /*
     * Custom ViewGroup Code
     */

    /**
     * {@inheritDoc}
     */
    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        final int specWidthSize = MeasureSpec.getSize(widthMeasureSpec);
        final int specWidthMode = MeasureSpec.getMode(widthMeasureSpec);
        final int specHeightSize = MeasureSpec.getSize(heightMeasureSpec);
        final int specHeightMode = MeasureSpec.getMode(heightMeasureSpec);

        //We expect to be somewhere inside a MaterialCalendarView, which should measure EXACTLY
        if (specHeightMode == MeasureSpec.UNSPECIFIED || specWidthMode == MeasureSpec.UNSPECIFIED) {
            throw new IllegalStateException("CalendarPagerView should never be left to decide it's size");
        }

        //The spec width should be a correct multiple
        final int measureTileWidth = specWidthSize / DEFAULT_DAYS_IN_WEEK;
        final int measureTileHeight = specHeightSize / (getRows() + (showWeekDays ? DAY_NAMES_ROW : 0));

        //Just use the spec sizes
        setMeasuredDimension(specWidthSize, specHeightSize);

        int count = getChildCount();

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);

            int childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(
                    measureTileWidth,
                    MeasureSpec.EXACTLY
            );

            int childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(
                    measureTileHeight,
                    MeasureSpec.EXACTLY
            );

            child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
        }
    }

    /**
     * Return the number of rows to display per page
     * @return
     */
    protected abstract int getRows();
    
    /**
     * 返回当前视图的模式
     * @return 参考{@link CalendarMode}
     */
    protected abstract CalendarMode getMode();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final int count = getChildCount();

        final int parentLeft = 0;

        int childTop = 0;
        int childLeft = parentLeft;

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);

            final int width = child.getMeasuredWidth();
            final int height = child.getMeasuredHeight();

            child.layout(childLeft, childTop, childLeft + width, childTop + height);

            childLeft += width;

            //We should warp every so many children
            if (i % DEFAULT_DAYS_IN_WEEK == (DEFAULT_DAYS_IN_WEEK - 1)) {
                childLeft = parentLeft;
                childTop += height;
            }

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams();
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams;
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new LayoutParams();
    }


    @Override
    public void onInitializeAccessibilityEvent(@NonNull AccessibilityEvent event) {
        super.onInitializeAccessibilityEvent(event);
        event.setClassName(CalendarPagerView.class.getName());
    }

    @Override
    public void onInitializeAccessibilityNodeInfo(@NonNull AccessibilityNodeInfo info) {
        super.onInitializeAccessibilityNodeInfo(info);
        info.setClassName(CalendarPagerView.class.getName());
    }

    protected CalendarDay getFirstViewDay() {
        return firstViewDay;
    }

    /**
     * Simple layout params class for MonthView, since every child is the same size
     */
    protected static class LayoutParams extends MarginLayoutParams {

        /**
         * {@inheritDoc}
         */
        public LayoutParams() {
            super(WRAP_CONTENT, WRAP_CONTENT);
        }
    }
}
