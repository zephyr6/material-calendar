package com.github.naturs.calendar;

import android.support.annotation.NonNull;
import android.view.View;

import static com.github.naturs.calendar.MaterialCalendarView.showOtherMonths;
import static com.github.naturs.calendar.MaterialCalendarView.showOutOfRange;

/**
 *
 * Created by naturs on 2017/10/18.
 */
final class DayViewVisibleDecorator extends DayViewDecorator<View> implements View.OnClickListener {

    DayViewVisibleDecorator() {
        super(0); // the highest render
    }

    @Override
    protected boolean shouldDecorate(@NonNull View view, @NonNull DayViewInfo info) {
        return true;
    }

    @Override
    protected void decorate(@NonNull View view, @NonNull DayViewInfo info, int calendarDayKey) {
        view.setTag(R.id.day_view_info_tag, info);
        if (info == null) {
            view.setVisibility(View.INVISIBLE);
            return;
        }

        boolean isInMonth = info.isInMonth();
        boolean isInRange = info.isInRange();
        int showOtherDates = info.getShowOtherDates();

        boolean enabled = isInMonth && isInRange;

        view.setEnabled(isInRange);

        boolean showOtherMonths = showOtherMonths(showOtherDates);
        boolean showOutOfRange = showOutOfRange(showOtherDates) || showOtherMonths;

        boolean shouldBeVisible = enabled;

        if (!isInMonth && showOtherMonths) {
            shouldBeVisible = true;
        }

        if (!isInRange && showOutOfRange) {
            shouldBeVisible |= isInMonth;
        }

        view.setVisibility(shouldBeVisible ? View.VISIBLE : View.INVISIBLE);
    
        view.setOnClickListener(this);
    }
    
    @Override
    protected void invalidateChecked(@NonNull View view, @NonNull DayViewInfo info, int calendarDayKey) {
    
    }
    
    @Override
    public void onClick(View v) {
        onDateClick(v);
    }
}
