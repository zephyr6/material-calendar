package com.github.naturs.calendar;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;
import android.view.View;

import java.util.Calendar;

/**
 * Display a week of {@linkplain DayView}s and
 * seven {@linkplain WeekDayView}s.
 */
@Experimental
@SuppressLint("ViewConstructor")
public class WeekView extends CalendarPagerView {

    public WeekView(@NonNull MaterialCalendarView view,
                    CalendarDay firstViewDay,
                    int firstDayOfWeek,
                    boolean showWeekDays,
                    DayViewGenerator<? extends View> generator) {
        super(view, firstViewDay, firstDayOfWeek, showWeekDays, generator);
    }

    @Override
    protected void buildDayViews(SparseArrayCompat<View> dayViews, Calendar calendar) {
        for (int i = 0; i < DEFAULT_DAYS_IN_WEEK; i++) {
            addDayView(dayViews, calendar);
        }
    }

    @Override
    protected boolean isDayEnabled(CalendarDay day) {
        return true;
    }

    @Override
    protected int getRows() {
        return 1;
    }
    
    @Override
    protected CalendarMode getMode() {
        return CalendarMode.WEEKS;
    }
}
