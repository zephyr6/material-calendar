package com.github.naturs.calendar;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.SparseArrayCompat;
import android.view.View;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Decorate Day views with drawables and text manipulation
 */
public abstract class DayViewDecorator<V extends View> {

    /**
     * [0, 100] is for library used.
     */
    private static final int RETAIN_PRIORITY = 100;

    /**
     * check priority user defined is valid.
     *
     * @param priority user defined priority
     */
    static void checkUserPriority(int priority) {
        if (priority <= RETAIN_PRIORITY) {
            throw new IllegalArgumentException("Your DayViewDecorator priority must greater than " + RETAIN_PRIORITY + ".");
        }
    }

    /**
     * Priority for current render. The smaller value, the higher priority.
     */
    private final int priority;
    
    @Nullable
    private MaterialCalendarView mcv;

    public DayViewDecorator(@IntRange(from = 0) int priority) {
        this.priority = priority;
    }

    final int priority() {
        return priority;
    }

    void setMcv(MaterialCalendarView mcv) {
        this.mcv = mcv;
    }
    
    public void onDateClick(@NonNull View view) {
        if (mcv == null) {
            return;
        }
        DayViewInfo info = getInfoForDayView(view);
        if (info != null) {
            mcv.onDateClicked(info);
        }
    }

    /**
     * 获取每个DayView上设置的数据
     * @param view 每天的控件View
     * @return 设置的数据
     */
    protected final DayViewInfo getInfoForDayView(@NonNull View view) {
        return (DayViewInfo) view.getTag(R.id.day_view_info_tag);
    }
    
    /**
     * 获取每个DayView的calendar key值，其实就是带有的Calendar数据的hashcode值
     * @param dayView 每天的控件View
     * @return calendar key
     */
    protected final int getCalendarKey(@NonNull V dayView) {
        return getInfoForDayView(dayView).getDate().hashCode();
    }
    
    /**
     * 获取某一页的所有DayView控件，如果是周模式，就获取某周的DayView，否则获取某月的DayView。
     *
     * 注意：某一天的DayView控件不一定只有一个，因为同一天可能出现在多页中。
     *
     * @param calendarDay 月份或者周
     * @return 以 {@link CalendarDay#hashCode()} 为key值的View列表
     */
    protected final List<V> findDayViewsForPager(@NonNull CalendarDay calendarDay) {
        // 有可能ViewPager内容还未初始化的时候就调用到该方法了
        if (mcv == null) {
            return null;
        }
        ArrayDeque<CalendarPagerView> pagerViews = mcv.getPagerViews();
        if (pagerViews == null || pagerViews.isEmpty()) {
            return null;
        }
    
        CalendarDay startDay, endDay;
        Calendar calendar = Calendar.getInstance();
        calendarDay.copyTo(calendar);
        
        if (CalendarMode.MONTHS.equals(mcv.getCalendarMode())) {
            int year = calendarDay.getYear();
            int month = calendarDay.getMonth();
            startDay = CalendarDay.from(year, month, 1);
            endDay = CalendarDay.from(year, month, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        } else if (CalendarMode.WEEKS.equals(mcv.getCalendarMode())) {
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            calendar.add(Calendar.DAY_OF_WEEK, - (dayOfWeek - mcv.getFirstDayOfWeek()));
            startDay = CalendarDay.from(calendar);
            calendar.add(Calendar.DAY_OF_WEEK, 6);
            endDay = CalendarDay.from(calendar);
        } else {
            throw new IllegalStateException("not support for the calendar mode: " + mcv.getCalendarMode());
        }
        
        return findDayViewsForPager(startDay, endDay);
    }
    
    /**
     * 获取某一个时间段的控件View列表。
     *
     * 注意：某一天的View不一定只存在一个，因为多页可能存在同一天的数据。
     *
     * @param startDay 开始时间
     * @param endDay 结束时间
     * @return 控件列表
     */
    @SuppressWarnings("unchecked")
    protected final List<V> findDayViewsForPager(@NonNull CalendarDay startDay,
                                                 @NonNull CalendarDay endDay) {
        if (startDay.isAfter(endDay)) {
            throw new IllegalArgumentException(startDay + " is after in " + endDay);
        }
    
        if (mcv == null) {
            return null;
        }
        
        ArrayDeque<CalendarPagerView> pagerViews = mcv.getPagerViews();
        if (pagerViews == null || pagerViews.isEmpty()) {
            return null;
        }
        
        List<View> views = new ArrayList<>();
        
        for (CalendarPagerView pagerView : pagerViews) {
            pagerView.findRangeDayViews(startDay, endDay, views);
        }
        
        return (List<V>) views;
    }
    
    /**
     * 获取某一个月的所有DayView控件
     * @param monthDay 月份
     * @return 以 {@link CalendarDay#hashCode()} 为key值的View列表
     */
    @Nullable
    protected final SparseArrayCompat<V> findDayViewsForMonth(@NonNull CalendarDay monthDay) {
        if (mcv == null) {
            return null;
        }
        
        if (!CalendarMode.MONTHS.equals(mcv.getCalendarMode())) {
            throw new IllegalStateException("must in months mode for this method.");
        }
        ArrayDeque<CalendarPagerView> pagerViews = mcv.getPagerViews();
        if (pagerViews == null || pagerViews.isEmpty()) {
            return null;
        }
    
        final int year = monthDay.getYear();
        final int month = monthDay.getMonth();
    
        for (CalendarPagerView pagerView : pagerViews) {
            CalendarDay firstDay = pagerView.getFirstViewDay();
            if (year == firstDay.getYear() && month == firstDay.getMonth()) {
                return pagerView.cloneDayViews();
            }
        }
    
        return null;
    }
    
    /**
     * 获取某一个周的所有DayView控件
     * @param weekDay 可以为某周里的任意一天
     * @return 以 {@link CalendarDay#hashCode()} 为key值的View列表
     */
    @Nullable
    protected final SparseArrayCompat<V> findDayViewsForWeek(@NonNull CalendarDay weekDay) {
        if (mcv == null) {
            return null;
        }
        
        if (!CalendarMode.WEEKS.equals(mcv.getCalendarMode())) {
            throw new IllegalStateException("must in week mode for this method.");
        }
        ArrayDeque<CalendarPagerView> pagerViews = mcv.getPagerViews();
        if (pagerViews == null || pagerViews.isEmpty()) {
            return null;
        }
    
        final int year = weekDay.getYear();
        final int month = weekDay.getMonth();
        final int weekOfYear = weekDay.getCalendar().get(Calendar.WEEK_OF_YEAR);
    
        for (CalendarPagerView pagerView : pagerViews) {
            CalendarDay firstDay = pagerView.getFirstViewDay();
            if (year == firstDay.getYear() && month == firstDay.getMonth() &&
                    weekOfYear == firstDay.getCalendar().get(Calendar.WEEK_OF_YEAR)) {
                return pagerView.cloneDayViews();
            }
        }
    
        return null;
    }

    /**
     * Determine if a specific day should be decorated
     *
     * @param view View to decorate
     * @param info {@linkplain DayViewInfo} to possibly decorate
     * @return true if this decorator should be applied to the provided day
     */
    protected boolean shouldDecorate(@NonNull V view, @NonNull DayViewInfo info) {
        return view.getVisibility() == View.VISIBLE;
    }

    /**
     * Set decoration options onto a facade to be applied to all relevant days
     *
     * @param view View to decorate
     */
    protected abstract void decorate(@NonNull V view, @NonNull DayViewInfo info, int calendarDayKey);
    
    /**
     * 更新日期的选中状态
     */
    protected abstract void invalidateChecked(@NonNull V view, @NonNull DayViewInfo info, int calendarDayKey);
}
